# FileCompression

This is a toy implementation of a custom file compression / decompression utility using Huffman encoding. It was created over a couple of evenings. Do not use it for anything serious :)

## Build & Test Locally
```bash
git clone git@gitlab.com:acizmarik/filecompression.git

# Requires .NET SDK 5.x
cd filecompression
dotnet build

# Run unit tests
cd Compression.Tests
dotnet test

# Test compress / decompress file
cd ..
dotnet run compress <original.txt> <compressed>
dotnet run decompress <compressed> <decompressed.txt>
```

## How does it Work?

### File Format
File format is custom and (probably) does not follow anything common. The compressed file is composed in the following way:
* File header `{huf;v1}` which is checked to determine if this utility was used for compression
* Compressed file block
   * `uint` **Size** (compressed file block length in bytes)
   * `uint` **Size** (size of original file block length in bytes)
   * `byte[]` Serialized **Huffman tree**
   * `byte[]` **Encoded data**

Note: whole program uses `GzipStream` so there is something more in the beginning. Although it can be removed and just compressed / decompressed using regular streams. Also the compression level for the `GzipStream` is set to `NoCompression` as I am doing my own Huffman encoding.

### Compression

Whole compression can be described as 3 concurrent tasks: **read**, **compress**, **write**.

* The **read** task:
   - Sequentially reads from input file and splits input into 1MB file chunks
   - File chunks are inserted into a `BlockingCollection`
* The **compression** task:
   - Consumes the `BlockingCollection` from the previous step in parallel using Task Parallel Library (TPL)
   - For each file block does the following:
      1) Calculate occurrences of bytes in the input (frequencies for individual bytes found in file)
      2) Construct Huffman tree
      3) Encode file block using Huffman tree
   - Encoded file blocks are then insered into another `BlockingCollection`
* The **write** task:
   - Consumes the `BlockingCollection` from the previous step sequentially
   - Sorts file blocks using heap to ensure they are written to file in the correct order

**Note**: whole compression works internally using fixed-sized byte buffers. It can be faster if more memory is made available to the pipeline. However, on my machine (latop with i7-8550U @1.8GHz) these settings seem to perform just fine and do not consume too much memory as well.

### Decompression

At this point it works only sequentially. Reads each compressed block, deserialize Huffman tree and decode the rest.
**Note**: this can be made into a parallel pipeline analogically to the compression part. I just did not have much time for that yet...
