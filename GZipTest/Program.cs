﻿using Compression;
using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;
using System.Threading.Tasks;

// Command definition for: GZipTest.exe compress
var compressionCommand = new Command("compress", "Compresses a file on input.")
{
    new Argument<string>("input", "Input file path."),
    new Argument<string>("output", "Output file path.")
};
compressionCommand.Handler = CommandHandler.Create<string, string>(async (input, output) => await CompressFile(input, output));

// Command definition for: GZipTest.exe decompress
var decompressionCommand = new Command("decompress", "Decompress a file on input.")
{
    new Argument<string>("input", "Input file path."),
    new Argument<string>("output", "Output file path.")
};
decompressionCommand.Handler = CommandHandler.Create<string, string>(DecompressFile);

// Build CLI
var rootCommand = new RootCommand(@"This is a utility for compression/decompression of files.
Note: this is a toy implementation. Do not use for anything serious :)");
rootCommand.Handler = CommandHandler.Create(() => rootCommand.Invoke("-h"));
rootCommand.AddCommand(compressionCommand);
rootCommand.AddCommand(decompressionCommand);

// Process args
return await rootCommand.InvokeAsync(args);

/// <summary>
/// Implements compression routine
/// </summary>
static async Task<int> CompressFile(string input, string output)
{
    try
    {
        using var compressor = new Compressor(input, output);
        await compressor.Compress();
    }
    catch (Exception ex)
    {
        Console.Error.WriteLine($"An error occurred during decompression. {ex.Message}");
        return 1;
    }

    return 0;
}

/// <summary>
/// Implements decompression routine
/// </summary>
static int DecompressFile(string input, string output)
{
    try
    {
        using var decompressor = new Decompressor(input, output);
        decompressor.Decompress();
    }
    catch (Exception ex)
    {
        Console.Error.WriteLine($"An error occurred during decompression. {ex.Message}");
        return 1;
    }

    return 0;
}