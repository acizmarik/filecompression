﻿using Compression.Huffman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Compression.Tests
{
    public class HuffmanTreeTests
    {
        [Fact]
        public void HuffmanTree_Build()
        {
            //             +-----+
            //             | (6) |
            //             |     |
            //             +--+--+
            //                |
            //                |
            //      +-----+---+---+-----+
            //      | (3) |       | (3) |
            //      |('A')|       |     |
            //      +-----+       +--+--+
            //                       |
            //                       |
            //             +-----+---+---+-----+
            //             | (1) |       | (2) |
            //             |('C')|       |('B')|
            //             +-----+       +-----+

            var text = "AAABBC";
            var bytes = Encoding.UTF8.GetBytes(text);

            var tree = new HuffmanTreeBuilder()
                .AddSymbolsInfo(GenerateOccurrences(bytes))
                .Build();
            var root = tree.Root;

            // Root properties
            Assert.NotNull(root);
            Assert.False(root.IsLeaf);
            Assert.Equal(bytes.Length, root.Weight);

            // Left child is a leaf node with weight=3 and character='A'
            var currentParent = root as HuffmanInternalNode;
            Assert.NotNull(currentParent.LeftChild);
            Assert.True(currentParent.LeftChild.IsLeaf);
            Assert.Equal(3, currentParent.LeftChild.Weight);
            Assert.Equal(65, ((HuffmanLeafNode)currentParent.LeftChild).Symbol);

            // Right child is an internal node with weight=3
            Assert.NotNull(currentParent.RightChild);
            Assert.False(currentParent.RightChild.IsLeaf);
            Assert.Equal(3, currentParent.RightChild.Weight);
            {
                currentParent = currentParent.RightChild as HuffmanInternalNode;

                // Its left child is a leaf node with weight=1 and character='C'
                Assert.NotNull(currentParent.LeftChild);
                Assert.True(currentParent.LeftChild.IsLeaf);
                Assert.Equal(1, currentParent.LeftChild.Weight);
                Assert.Equal(67, ((HuffmanLeafNode)currentParent.LeftChild).Symbol);

                // Its right child is a leaf node with weight=2 and character='B'
                Assert.NotNull(currentParent.RightChild);
                Assert.True(currentParent.RightChild.IsLeaf);
                Assert.Equal(2, currentParent.RightChild.Weight);
                Assert.Equal(66, ((HuffmanLeafNode)currentParent.RightChild).Symbol);
            }
        }

        [Theory]
        [InlineData("AAABBC", new[] { /* A */ 65, /* B */ 66, /* C */ 67 }, new[] { "0", "11", "1" })]
        public void HuffmanTree_EncodingMap(string text, int[] symbols, string[] encodings)
        {
            var bytes = Encoding.UTF8.GetBytes(text);

            var tree = new HuffmanTreeBuilder()
                .AddSymbolsInfo(GenerateOccurrences(bytes))
                .Build();

            for (var index = 0; index < symbols.Length; index++)
            {
                var (encoding, _) = tree.GetTranslation((byte)symbols[index]);
                Assert.Equal(encodings[index], Convert.ToString((long)encoding, 2));
            }
        }

        [Theory]
        [InlineData("aaabbc")]
        [InlineData("abcdefghijklomnopqrstuvz")]
        [InlineData("aaaaaaaaaabbbbbbbbbccccccccdddddddeeeeeefffffgggghhhiij")]
        public void HuffmanTree_DeconstructedTreeIsSame(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var tree = new HuffmanTreeBuilder()
                .AddSymbolsInfo(GenerateOccurrences(bytes))
                .Build();

            var spanIndex = 0;
            var bitIndex = 0;
            Span<byte> buffer = stackalloc byte[100];
            Compressor.WriteTree(buffer, ref spanIndex, ref bitIndex, tree);
            var size = spanIndex;

            spanIndex = 0;
            bitIndex = 0;
            var deconstructed = Decompressor.ReadTree(buffer, ref spanIndex, ref bitIndex);

            EnsureHaveSameSymbols(tree, deconstructed);
        }

        internal static int[] GenerateOccurrences(byte[] text)
        {
            var counters = new int[256];

            // Calculate occurrences for individual symbols
            for (var index = 0; index < text.Length; index++)
            {
                var currentSymbol = text[index];
                counters[currentSymbol]++;
            }

            return counters;
        }

        private static void EnsureHaveSameSymbols(HuffmanTree first, HuffmanTree second)
        {
            var queue = new Queue<(IHuffmanNode, IHuffmanNode)>();
            queue.Enqueue((first.Root, second.Root));

            while (queue.Count > 0)
            {
                var (nodeA, nodeB) = queue.Dequeue();
                if (nodeA.IsLeaf)
                {
                    // Assert second is leaf and symbols are same
                    Assert.IsType<HuffmanLeafNode>(nodeB);
                    Assert.Equal(((HuffmanLeafNode)nodeA).Symbol, ((HuffmanLeafNode)nodeB).Symbol);
                }
                else
                {
                    // Assert second is internal
                    Assert.IsType<HuffmanInternalNode>(nodeB);
                    queue.Enqueue((((HuffmanInternalNode)nodeA).LeftChild, ((HuffmanInternalNode)nodeB).LeftChild));
                    queue.Enqueue((((HuffmanInternalNode)nodeA).RightChild, ((HuffmanInternalNode)nodeB).RightChild));
                }
            }
        }
    }
}
