﻿using Compression.Huffman;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Compression.Tests
{
    public class HuffmanNodesOrderingTests
    {
        [Fact]
        public void HuffmanNodes_CompareLeaves_DifferentWeights()
        {
            var leaf1 = new HuffmanLeafNode(65, 3);
            var leaf2 = new HuffmanLeafNode(66, 2);

            // Leaf1 > Leaf2
            Assert.True(leaf1.CompareTo(leaf2) == 1);
        }

        [Fact]
        public void HuffmanNodes_CompareLeaves_SameWeights()
        {
            var leaf1 = new HuffmanLeafNode(65, 3);
            var leaf2 = new HuffmanLeafNode(66, 3);

            // Leaf1 < Leaf2
            Assert.True(leaf1.CompareTo(leaf2) == -1);
        }

        [Fact]
        public void HuffmanNodes_CompareInternals_DifferentWeights()
        {
            var leaf1 = new HuffmanInternalNode(new HuffmanLeafNode(65, 2), new HuffmanLeafNode(66, 2), 1);
            var leaf2 = new HuffmanInternalNode(new HuffmanLeafNode(66, 1), new HuffmanLeafNode(67, 2), 2);

            // Leaf1 > Leaf2
            Assert.True(leaf1.CompareTo(leaf2) == 1);
        }

        [Fact]
        public void HuffmanNodes_CompareInternals_SameWeights()
        {
            var leaf1 = new HuffmanInternalNode(new HuffmanLeafNode(65, 2), new HuffmanLeafNode(66, 2), 1);
            var leaf2 = new HuffmanInternalNode(new HuffmanLeafNode(66, 2), new HuffmanLeafNode(67, 2), 2);

            // Leaf1 < Leaf2
            Assert.True(leaf1.CompareTo(leaf2) == -1);
        }

        [Fact]
        public void HuffmanNodes_CompareLeafWithInternal()
        {
            var leaf1 = new HuffmanLeafNode(65, 3);
            var leaf2 = new HuffmanInternalNode(new HuffmanLeafNode(67, 1), new HuffmanLeafNode(66, 2), 1);

            // Leaf1 < Leaf2
            Assert.True(leaf1.CompareTo(leaf2) == -1);
        }

        [Fact]
        public void HuffmanNodes_CompareInternalWithLeaf()
        {
            var leaf1 = new HuffmanLeafNode(65, 3);
            var leaf2 = new HuffmanInternalNode(new HuffmanLeafNode(67, 1), new HuffmanLeafNode(66, 2), 1);

            // Leaf1 < Leaf2
            Assert.True(leaf2.CompareTo(leaf1) == 1);
        }
    }
}
