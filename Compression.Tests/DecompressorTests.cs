﻿using System;
using System.Buffers;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Compression.Tests
{
    public class DecompressorTests
    {
        [Fact]
        public static void DecompressorTests_Simple_ReadTree()
        {
            // Serialized tree for text: aaabbc
            ReadOnlySpan<byte> compressed = stackalloc byte[4] { 134, 57, 86, 12 };
            var spanIndex = 0;
            var bitIndex = 0;

            // Deserialize tree
            var tree = Decompressor.ReadTree(compressed, ref spanIndex, ref bitIndex);

            Assert.NotNull(tree);
            Assert.NotNull(tree.Root);

            // Get translation for 'a'
            var (aEncoding, aEncodingLength) = tree.GetTranslation(Encoding.UTF8.GetBytes("a").First());
            Assert.Equal(0u, aEncoding);
            Assert.Equal(1, aEncodingLength);

            // Get translation for 'b'
            var (bEncoding, bEncodingLength) = tree.GetTranslation(Encoding.UTF8.GetBytes("b").First());
            Assert.Equal(3u, bEncoding);
            Assert.Equal(2, bEncodingLength);

            // Get translation for 'c'
            var (cEncoding, cEncodingLength) = tree.GetTranslation(Encoding.UTF8.GetBytes("c").First());
            Assert.Equal(1u, cEncoding);
            Assert.Equal(2, cEncodingLength);
        }

        [Fact]
        public static void DecompresorTests_Simple_ReadBlock()
        {
            ReadOnlySpan<byte> compressed = stackalloc byte[9] 
            { 
                6, 0, 0, 0 /* data length */,
                0b0000_1100, 0b0111_0011, 0b1010_1100, 0b0001_1000, 0b0011_1110 /* data */
            };

            var (buffer, length, isLast) = Decompressor.DecompressBlock(ArrayPool<byte>.Shared, compressed);

            Assert.NotNull(buffer);
            Assert.Equal(6, length);
            Assert.Equal("aaabbc", Encoding.UTF8.GetString(buffer, 0, length));
            Assert.True(isLast);

            ArrayPool<byte>.Shared.Return(buffer);
        }
    }
}
