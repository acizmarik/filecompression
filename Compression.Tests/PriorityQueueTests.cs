using Compression.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Compression.Tests
{
    public class PriorityQueueTests
    {
        [Fact]
        public void PriorityQueue_Min_Build()
        {
            var queue = PriorityQueue<int>.CreateMinimum();

            Assert.Equal(0, queue.Count);
        }

        [Fact]
        public void PriorityQueue_Min_EnqueueElement()
        {
            var queue = PriorityQueue<int>.CreateMinimum();
            queue.Enqueue(123);

            Assert.Equal(1, queue.Count);
            Assert.Equal(123, queue.Peek());
        }

        [Fact]
        public void PriorityQueue_Min_DequeueElement()
        {
            var queue = PriorityQueue<int>.CreateMinimum();
            queue.Enqueue(123);
            var result = queue.Dequeue();

            Assert.Equal(0, queue.Count);
            Assert.Equal(123, result);
        }

        [Fact]
        public void PriorityQueue_Min_Clear()
        {
            var queue = PriorityQueue<int>.CreateMinimum();
            queue.Enqueue(123);
            queue.Clear();

            Assert.Equal(0, queue.Count);
        }

        [Fact]
        public void PriorityQueue_Min_Throws_WhenEmptyDequeue()
        {
            var queue = PriorityQueue<int>.CreateMinimum();

            Assert.Throws<InvalidOperationException>(() => queue.Dequeue());
        }

        [Fact]
        public void PriorityQueue_Min_Throws_WhenEmptyPeek()
        {
            var queue = PriorityQueue<int>.CreateMinimum();

            Assert.Throws<InvalidOperationException>(() => queue.Peek());
        }

        [Theory]
        [InlineData(new[] { 3, 10, 8, 4, 9, 2, 6, 5, 7, 1 })]
        [InlineData(new[] { 8, 3, 6, 10, 5, 2, 9, 7, 4, 1 })]
        [InlineData(new[] { 2, 8, 6, 1, 3, 7, 5, 9, 4, 10 })]
        public void PriorityQueue_Min_AddUnsortedElements(int[] collection)
        {
            var queue = PriorityQueue<int>.CreateMinimum();
            var sortedCollection = collection.OrderBy(i => i).ToList();

            foreach (var item in collection)
                queue.Enqueue(item);

            Assert.Equal(collection.Length, queue.Count);

            var counter = 0;
            while (queue.Count > 0)
                Assert.Equal(sortedCollection[counter++], queue.Dequeue());
        }

        [Theory]
        [InlineData(new[] { 3, 10, 8, 4, 9, 2, 6, 5, 7, 1 })]
        [InlineData(new[] { 8, 3, 6, 10, 5, 2, 9, 7, 4, 1 })]
        [InlineData(new[] { 2, 8, 6, 1, 3, 7, 5, 9, 4, 10 })]
        public void PriorityQueue_Max_AddUnsortedElements(int[] collection)
        {
            var queue = PriorityQueue<int>.CreateMaximum();
            var sortedCollection = collection.OrderByDescending(i => i).ToList();

            foreach (var item in collection)
                queue.Enqueue(item);

            Assert.Equal(collection.Length, queue.Count);

            var counter = 0;
            while (queue.Count > 0)
                Assert.Equal(sortedCollection[counter++], queue.Dequeue());
        }
    }
}
