﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Compression.Utilities.ByteSpanExtensions;

namespace Compression.Tests
{
    public class ByteSpanReadExtensions
    {
        [Fact]
        public static void ByteSpanExtensions_Read_Simple_BitRead()
        {
            Span<byte> bytes = stackalloc byte[3];
            bytes[0] = 0b1010_1010;
            ReadOnlySpan<byte> toRead = bytes;

            var spanIndex = 0;
            var bitIndex = 0;

            Assert.Equal(BitValue.Low, toRead.ReadBit(ref spanIndex, ref bitIndex));
            Assert.Equal(0, spanIndex);
            Assert.Equal(1, bitIndex);

            Assert.Equal(BitValue.High, toRead.ReadBit(ref spanIndex, ref bitIndex));
            Assert.Equal(0, spanIndex);
            Assert.Equal(2, bitIndex);
        }

        [Fact]
        public static void ByteSpanExtensions_Read_Simple_ByteRead()
        {
            Span<byte> bytes = stackalloc byte[3];
            bytes[0] = 121;
            ReadOnlySpan<byte> toRead = bytes;
            var spanIndex = 0;
            var bitIndex = 0;

            Assert.Equal(121, toRead.ReadByte(ref spanIndex, ref bitIndex));
            Assert.Equal(1, spanIndex);
            Assert.Equal(0, bitIndex);
        }

        [Fact]
        public static void ByteSpanExtensions_Read_OverMultipleBytes_ByteRead()
        {
            Span<byte> bytes = stackalloc byte[3];
            bytes[0] = 121;
            bytes[1] = 1;
            ReadOnlySpan<byte> toRead = bytes;
            var spanIndex = 0;
            var bitIndex = 0;

            toRead.ReadBit(ref spanIndex, ref bitIndex);
            Assert.Equal(0b1011_1100, toRead.ReadByte(ref spanIndex, ref bitIndex));
            Assert.Equal(1, spanIndex);
            Assert.Equal(1, bitIndex);
        }
    }
}
