﻿using Compression.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Compression.Utilities.ByteSpanExtensions;

namespace Compression.Tests
{
    public class ByteSpanWriteExtensions
    {
        [Fact]
        public static void ByteSpanExtensions_Write_Simple_BitWriteHigh()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);

            Assert.Equal(0, spanIndex);
            Assert.Equal(1, bitIndex);
            Assert.Equal(1, bytes[0]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_Simple_BitWriteLow()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);

            Assert.Equal(0, spanIndex);
            Assert.Equal(1, bitIndex);
            Assert.Equal(0, bytes[0]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_WholeByte_BitWrite()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);

            Assert.Equal(1, spanIndex);
            Assert.Equal(0, bitIndex);
            Assert.Equal(17, bytes[0]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_Simple_WriteByte()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteByte(121, ref spanIndex, ref bitIndex);

            Assert.Equal(1, spanIndex);
            Assert.Equal(0, bitIndex);
            Assert.Equal(121, bytes[0]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_OverMultipleBytes_WriteByte()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteByte(0b1001_1001 /* 153 */, ref spanIndex, ref bitIndex);

            Assert.Equal(1, spanIndex);
            Assert.Equal(1, bitIndex);
            Assert.Equal(0b0011_0011 /* 51 */, bytes[0]);
            Assert.Equal(0b0000_0001 /* 1 */, bytes[1]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_Simple_WriteBits()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBits(0b1111_1111, length: 4, ref spanIndex, ref bitIndex);

            Assert.Equal(0, spanIndex);
            Assert.Equal(8, bitIndex);
            Assert.Equal(0b1111_0101, bytes[0]);
        }

        [Fact]
        public static void ByteSpanExtensions_Write_OverMultipleBytes_WriteBits()
        {
            Span<byte> bytes = stackalloc byte[3];
            var spanIndex = 0;
            var bitIndex = 0;

            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
            bytes.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
            bytes.WriteBits(0b1111_1111, length: 6, ref spanIndex, ref bitIndex);

            Assert.Equal(1, spanIndex);
            Assert.Equal(2, bitIndex);
            Assert.Equal(0b1111_0101, bytes[0]);
            Assert.Equal(0b0000_0011, bytes[1]);
        }
    }
}
