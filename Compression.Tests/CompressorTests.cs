﻿using Compression.Huffman;
using Compression.Utilities;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using static Compression.Utilities.ByteSpanExtensions;

namespace Compression.Tests
{
    public class CompressorTests
    {
        [Fact]
        public static void CompressorTests_Simple_WriteTree()
        {
            // Compress data
            const string text = "aaabbc";
            var inputBytes = Encoding.UTF8.GetBytes(text);
            var arrayPool = ArrayPool<byte>.Shared;
            var occurrences = HuffmanTreeTests.GenerateOccurrences(inputBytes);
            var tree = new HuffmanTreeBuilder()
                .AddSymbolsInfo(occurrences)
                .Build();

            // Write tree
            var spanIndex = 0;
            var bitIndex = 0;
            Span<byte> buffer = stackalloc byte[10];
            Compressor.WriteTree(buffer, ref spanIndex, ref bitIndex, tree);

            Assert.Equal(0b1000_0110 /* 134 */, buffer[0]);
            Assert.Equal(0b0011_1001 /* 57 */, buffer[1]);
            Assert.Equal(0b0101_0110 /* 86 */, buffer[2]);
            Assert.Equal(0b0000_1100 /* 12 */, buffer[3]);
        }

        [Theory]
        [InlineData("aaabbc")]
        [InlineData("abcdefghijklomnopqrstuvz")]
        [InlineData("aaaaaaaaaabbbbbbbbbccccccccdddddddeeeeeefffffgggghhhiij")]
        [InlineData(@"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")]

        public static void CompressorTests_Block_CompressDecompress_AreEqual(string text)
        {
            // Compress data
            var inputBytes = Encoding.UTF8.GetBytes(text);
            var occurrences = HuffmanTreeTests.GenerateOccurrences(inputBytes);
            var tree = new HuffmanTreeBuilder()
                .AddSymbolsInfo(occurrences)
                .Build();
            // Write block
            var data = Compressor.CompressBlock(new byte[1024], inputBytes, true, tree);

            var (decompressed, length, isLast) = Decompressor.DecompressBlock(ArrayPool<byte>.Shared, data);
            var result = Encoding.UTF8.GetString(decompressed, 0, length);
            Assert.Equal(text, result);
        }
    }
}
