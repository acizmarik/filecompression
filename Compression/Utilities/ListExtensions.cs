﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Utilities
{
    internal static class ListExtensions
    {
        public static void RemoveLastElement<T>(this List<T> list)
        {
            if (list.Count == 0)
                throw new InvalidOperationException("Can not remove from empty list.");

            list.RemoveAt(list.GetIndexOfLastElement());
        }

        public static int GetIndexOfLastElement<T>(this List<T> list)
        {
            return list.Count - 1;
        }
    }
}
