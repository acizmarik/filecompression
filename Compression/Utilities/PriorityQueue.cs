﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Utilities
{
    /// <summary>
    /// Basic minimum/maximum binary heap implementation
    /// </summary>
    public class PriorityQueue<T> where T : IComparable<T>
    {
        public int Count => data.Count;

        private const int defaultCapacity = 4;
        private Func<T, T, int> comparator;
        private List<T> data;

        public static PriorityQueue<T> CreateMinimum()
            => CreateMinimum(defaultCapacity);

        public static PriorityQueue<T> CreateMinimum(int capacity)
            => new((first, second) => first.CompareTo(second), capacity);

        public static PriorityQueue<T> CreateMaximum()
            => CreateMaximum(defaultCapacity);

        public static PriorityQueue<T> CreateMaximum(int capacity)
            => new((first, second) => second.CompareTo(first), capacity);

        private PriorityQueue(Func<T, T, int> comparator, int capacity)
        {
            this.data = new List<T>(capacity);
            this.comparator = comparator;
        }

        public void Enqueue(T element)
        {
            data.Add(element);
            var index = data.GetIndexOfLastElement();

            // Bubble up
            while (index > 0)
            {
                var parentIndex = (index - 1) / 2;

                if (comparator(data[parentIndex], data[index]) == 1)
                {
                    // Element needs to be swapped with its parent
                    Swap(index, parentIndex);
                    index = parentIndex;
                }
                else
                {
                    // Elements are in correct order
                    // Heap is in consistent state
                    break;
                }
            }
        }

        public T Dequeue()
        {
            if (data.Count == 0)
                throw new InvalidOperationException($"Can not call {nameof(Dequeue)} on an empty priority queue.");

            var result = data[0];
            Swap(0, data.GetIndexOfLastElement());
            data.RemoveLastElement();

            // Bubble down
            var index = 0;
            while (index * 2 + 1 < data.Count)
            {
                var indexLeftChild = index * 2 + 1;
                var indexRightChild = index * 2 + 2;

                // Select index of child to test
                var childIndex = (indexRightChild == data.Count || comparator(data[indexRightChild], data[indexLeftChild]) == 1) 
                    ? indexLeftChild : indexRightChild;

                if (comparator(data[index], data[childIndex]) == 1)
                {
                    // Element needs to be swapped with the child
                    Swap(index, childIndex);
                    index = childIndex;
                }
                else
                {
                    // Elements are in correct order
                    // Heap is in consistent state
                    break;
                }
            }

            return result;
        }

        public T Peek()
        {
            if (data.Count == 0)
                throw new InvalidOperationException($"Can not call {nameof(Peek)} on an empty priority queue.");

            return data[0];
        }

        private void Swap(int index1, int index2)
        {
            var temp = data[index1];
            data[index1] = data[index2];
            data[index2] = temp;
        }

        public void Clear()
        {
            data.Clear();
        }
    }
}
