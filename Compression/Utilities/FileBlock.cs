﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Utilities
{
    public struct FileBlock : IComparable<FileBlock>
    {
        public readonly int Index;
        public readonly byte[] Data;
        public readonly int Start;
        public readonly int Length;
        public readonly int BufferIndex;

        public FileBlock(int index, byte[] data, int start, int length, int bufferIndex)
        {
            this.Index = index;
            this.Data = data;
            this.Start = start;
            this.Length = length;
            this.BufferIndex = bufferIndex;
        }

        public int CompareTo(FileBlock other)
        {
            return (Index < other.Index) ? -1 :
                (Index > other.Index) ? 1 : 0;
        }

        public override string ToString()
        {
            return $"Block index: {Index}";
        }
    }
}
