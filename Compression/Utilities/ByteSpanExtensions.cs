﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Utilities
{
    public static class ByteSpanExtensions
    {
        public enum BitValue { Low = 0, High = 1 }

        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
        public static void WriteBit(this Span<byte> bytes, BitValue value, ref int spanIndex, ref int bitIndex)
        {
            // Create and apply mask
            var mask = (byte)((int)value << bitIndex++);
            bytes[spanIndex] |= mask;

            // Fix indices
            spanIndex = (bitIndex == 8) ? spanIndex + 1 : spanIndex;
            bitIndex = (bitIndex == 8) ? 0 : bitIndex;
        }

        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
        public static BitValue ReadBit(this ReadOnlySpan<byte> bytes, ref int spanIndex, ref int bitIndex)
        {
            // Create and apply mask
            var result = ((bytes[spanIndex] & (1 << bitIndex)) > 0) ? BitValue.High : BitValue.Low;

            // Fix indices
            bitIndex++;
            spanIndex = (bitIndex == 8) ? spanIndex + 1 : spanIndex;
            bitIndex = (bitIndex == 8) ? 0 : bitIndex;

            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
        public static void WriteByte(this Span<byte> bytes, byte value, ref int spanIndex, ref int bitIndex)
        {
            // Create and apply mask1
            var mask1 = (byte)(value & (255 >> bitIndex));
            bytes[spanIndex] |= (byte)(mask1 << bitIndex);

            spanIndex++;
            value >>= (8 - bitIndex);

            bytes[spanIndex] |= value;
        }

        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
        public static byte ReadByte(this ReadOnlySpan<byte> bytes, ref int spanIndex, ref int bitIndex)
        {
            // Create and apply mask1
            var mask1 = (byte)(bytes[spanIndex] & 0xFF);
            var result = (byte)(mask1 >> bitIndex);

            if (++spanIndex >= bytes.Length)
                return result;

            // Create and apply mask2
            var mask2 = (byte)(bytes[spanIndex] & (255 >> (8 - bitIndex)));
            result |= (byte)(mask2 << (8 - bitIndex));

            return result;
        }

        [MethodImpl(MethodImplOptions.AggressiveOptimization)]
        public static void WriteBits(this Span<byte> bytes, byte value, int length, ref int spanIndex, ref int bitIndex)
        {
            // Create mask
            var mask = (byte)(value & (255 >> (8 - length)));

            if (length + bitIndex <= 8)
            {
                bytes[spanIndex] |= (byte)(mask << bitIndex);
                bitIndex += length;
            }
            else
            {
                var mask1Size = 8 - bitIndex;
                var mask1 = (byte)(mask & (255 >> (8 - mask1Size)));
                bytes[spanIndex] |= (byte)(mask1 << bitIndex);
                spanIndex++;

                var mask2 = (byte)(mask >> mask1Size);
                bytes[spanIndex] |= mask2;
                bitIndex = length - mask1Size;
            }
        }
    }
}
