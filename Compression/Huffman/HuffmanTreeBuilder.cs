﻿using Compression.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Huffman
{
    internal class HuffmanTreeBuilder
    {
        private PriorityQueue<IHuffmanNode>? queue;
        
        public HuffmanTreeBuilder AddSymbolsInfo(ReadOnlySpan<int> symbolOccurrences)
        {
            queue = PriorityQueue<IHuffmanNode>.CreateMinimum();
            for (var index = 0; index < symbolOccurrences.Length; index++)
            {
                if (symbolOccurrences[index] == 0)
                    continue;

                queue.Enqueue(new HuffmanLeafNode((byte)index, symbolOccurrences[index]));
            }

            return this;
        }

        public HuffmanTreeBuilder AddSymbolsInfo(IEnumerable<KeyValuePair<byte, int>> symbolOccurrences)
        {
            queue = PriorityQueue<IHuffmanNode>.CreateMinimum();
            foreach (var (symbol, count) in symbolOccurrences)
                queue.Enqueue(new HuffmanLeafNode(symbol, count));

            return this;
        }

        public HuffmanTree Build()
        {
            if (queue == null)
                throw new InvalidOperationException("No symbols were provided to build the tree from.");

            var counter = 0;
            while (queue.Count >= 2)
            {
                var left = queue.Dequeue();
                var right = queue.Dequeue();
                queue.Enqueue(new HuffmanInternalNode(left, right, counter++));
            }

            return new(queue.Dequeue());
        }
    }
}
