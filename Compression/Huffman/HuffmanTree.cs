﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Huffman
{
    internal class HuffmanTree
    {
        public IHuffmanNode Root { get; }
        private Dictionary<byte, (ulong Data, int Length)> EncodingMap;

        public HuffmanTree(IHuffmanNode root)
        {
            this.Root = root;
            this.EncodingMap = new Dictionary<byte, (ulong Data, int Length)>();
            Initialize();
        }

        private void Initialize()
        {
            void Traverse(IHuffmanNode node, uint path, int length)
            {
                if (node is HuffmanInternalNode internalNode)
                {
                    // Traverse left sub-tree
                    Traverse(internalNode.LeftChild, (path << 1), length + 1);

                    // Traverse right sub-tree
                    Traverse(internalNode.RightChild, (path << 1) | 1, length + 1);
                }
                else
                {
                    var leaf = node as HuffmanLeafNode;
                    EncodingMap.Add(leaf!.Symbol, (GetReversedPath(path, length), length));
                }
            }

            Traverse(Root, 0u, 0);
        }

        private ulong GetReversedPath(ulong value, int length)
        {
            var result = 0UL;

            var bitIndex = 0;
            for (var i = length - 1; i >= 0; i--)
            {
                var isHigh = (value & (1UL << i)) > 0;
                result |= (((isHigh) ? 1UL : 0UL) << bitIndex++);
            }

            return result;
        }

        public (ulong Encoding, int Length) GetTranslation(byte input)
            => EncodingMap[input];
    }
}
