﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Huffman
{
    internal class HuffmanInternalNode : IHuffmanNode
    {
        public IHuffmanNode LeftChild { get; }
        public IHuffmanNode RightChild { get; }
        public bool IsLeaf => false;
        public int Weight { get; }
        public int NodeIndex { get; }

        public HuffmanInternalNode(IHuffmanNode left, IHuffmanNode right, int nodeIndex)
        {
            this.LeftChild = left;
            this.RightChild = right;
            this.Weight = LeftChild.Weight + RightChild.Weight;
            this.NodeIndex = nodeIndex;
        }

        public int CompareTo(IHuffmanNode? other)
        {
            if (other == null)
            {
                // Any object is greater than null
                return 1;
            }
            if (Weight != other.Weight)
            {
                return Weight.CompareTo(other.Weight);
            }
            else
            {
                if (other!.IsLeaf)
                {
                    return 1;
                }
                else
                {
                    // Compare internal nodes based on order of creation
                    var otherInternal = (other as HuffmanInternalNode)!;
                    return NodeIndex.CompareTo(otherInternal.NodeIndex);
                }
            }
        }

        public override string ToString()
        {
            return $"{{Internal: [Index:{NodeIndex}; Weight: {Weight}]}}";
        }
    }
}
