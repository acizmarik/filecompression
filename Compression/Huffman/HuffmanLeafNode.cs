﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Huffman
{
    internal class HuffmanLeafNode : IHuffmanNode
    {
        public bool IsLeaf => true;
        public int Weight { get; }
        public byte Symbol { get; }

        public HuffmanLeafNode(byte symbol, int weight)
        {
            this.Symbol = symbol;
            this.Weight = weight;
        }

        public int CompareTo(IHuffmanNode? other)
        {
            if (other == null)
            {
                // Any object is greater then null
                return 1;
            }
            if (Weight != other.Weight)
            {
                // Weight comparing has priority
                return Weight.CompareTo(other.Weight);
            }
            else
            {
                if (!other!.IsLeaf)
                {
                    // Leaves have priority
                    return -1;
                }
                else
                {
                    // Compare leave nodes based on order of symbols
                    var otherLeaf = (other as HuffmanLeafNode)!;
                    return Symbol.CompareTo(otherLeaf.Symbol);
                }
            }
        }

        public override string ToString()
        {
            return $"{{Leaf: [Symbol:{(char)Symbol}; Weight: {Weight}]}}";
        }
    }
}
