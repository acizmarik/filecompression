﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Compression.Huffman
{
    internal interface IHuffmanNode : IComparable<IHuffmanNode>
    {
        bool IsLeaf { get; }
        int Weight { get; }
    }
}
