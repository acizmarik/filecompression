﻿using Compression.Huffman;
using Compression.Utilities;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Compression.Utilities.ByteSpanExtensions;

namespace Compression
{
    public class Decompressor : IDisposable
    {
        public readonly byte[] Header = Encoding.UTF8.GetBytes("{huf;v1}");
        private const int defaultBufferSize = 1024 * 1024 + 512;
        private readonly string inputFileName;
        private readonly string outputFileName;
        private readonly Stream inputStream;
        private readonly Stream outputStream;
        private readonly GZipStream gzipStream;
        private readonly ArrayPool<byte> arrayPool;
        private bool isDisposed;

        public Decompressor(string input, string output)
        {
            this.inputFileName = input;
            this.outputFileName = output;
            this.inputStream = File.Open(input, FileMode.Open, FileAccess.Read);
            this.outputStream = File.Create(output, defaultBufferSize);
            this.gzipStream = new(inputStream, CompressionMode.Decompress);
            this.arrayPool = ArrayPool<byte>.Shared;
        }

        public Decompressor(Stream input, Stream output)
        {
            this.inputFileName = "NA";
            this.outputFileName = "NA";
            this.inputStream = input;
            this.outputStream = output;
            this.gzipStream = new(inputStream, CompressionMode.Decompress);
            this.arrayPool = ArrayPool<byte>.Shared;
        }

        public void Decompress()
        {
            var inputBuffer = new byte[defaultBufferSize];

            // Try read header
            gzipStream.Read(inputBuffer, 0, Header.Length + sizeof(uint));
            if (!ValidateHeader(inputBuffer, Header))
                throw new ArgumentException($"Invalid input file: {inputFileName}. This file was not compressed by this program!");
            var compressedSizeSource = new ReadOnlySpan<byte>(inputBuffer, Header.Length, sizeof(uint));

            // Read file content
            bool finish;
            do
            {
                // Get size of next compressed file block
                var compressedSize = MemoryMarshal.Read<uint>(compressedSizeSource);
                // Get next compressed block
                var toRead = compressedSize + sizeof(uint);
                var read = gzipStream.Read(inputBuffer, 0, (int)toRead);
                if (read < compressedSize)
                    throw new ArgumentException($"Corrupted input file: {inputFileName}.");
                compressedSizeSource = new ReadOnlySpan<byte>(inputBuffer, (int)compressedSize, sizeof(uint));

                // Decompress block
                var (buffer, length, isLast) = DecompressBlock(arrayPool, new(inputBuffer, 0, read));

                // Write to output
                outputStream.Write(buffer, 0, length);
                finish = isLast;
            } while (!finish);
        }

        private static bool ValidateHeader(ReadOnlySpan<byte> input, ReadOnlySpan<byte> header)
        {
            for (var i = 0; i < header.Length; i++)
            {
                if (input[i] != header[i])
                    return false;
            }

            return true;
        }

        internal static HuffmanTree ReadTree(ReadOnlySpan<byte> input, ref int spanIndex, ref int bitIndex)
        {
            var stack = new Stack<(Func<IHuffmanNode, IHuffmanNode, IHuffmanNode>? InternalBuilder, IHuffmanNode? Node)>();

            do
            {
                var value = input.ReadBit(ref spanIndex, ref bitIndex);
                if (value == BitValue.Low)
                {
                    // Reading internal node
                    stack.Push(((first, second) => new HuffmanInternalNode(first, second, -1), null));
                }
                else
                {
                    // Reading leaf node
                    var symbol = input.ReadByte(ref spanIndex, ref bitIndex);
                    IHuffmanNode current = new HuffmanLeafNode(symbol, -1);

                    // Check if we can build
                    while (stack.Count > 1 && stack.Peek().Node != null)
                    {
                        var previous = stack.Pop().Node!;
                        var activator = stack.Pop().InternalBuilder!;
                        current = activator(previous, current);
                    }

                    stack.Push((null, current));
                }
            } while (stack.Peek().Node == null || stack.Count > 1);

            // Root is on the top of stack
            return new(stack.Pop().Node!);
        }

        internal static (byte[] Buffer, int Length, bool IsLast) DecompressBlock(ArrayPool<byte> pool, ReadOnlySpan<byte> input)
        {
            var spanPosition = 0;
            var bitPosition = 0;

            // Read length of original data
            var length = (int)MemoryMarshal.Read<uint>(input);
            var output = pool.Rent(length);
            spanPosition += sizeof(uint);

            // Read flag indicating if there are more blocks
            var isLast = input.ReadBit(ref spanPosition, ref bitPosition) == BitValue.Low;

            // Read huffman tree
            var tree = ReadTree(input, ref spanPosition, ref bitPosition);

            // Decompress data
            var decompressedBytesCount = 0;
            var node = tree.Root;
            while (decompressedBytesCount < length)
            {
                var @byte = input.ReadByte(ref spanPosition, ref bitPosition);
                for (var index = 0; index < 8; index++)
                {
                    if ((@byte & (1 << index)) > 0)
                    {
                        // Bit is high
                        node = ((HuffmanInternalNode)node).RightChild;
                    }
                    else
                    {
                        // Bit is low
                        node = ((HuffmanInternalNode)node).LeftChild;
                    }

                    if (node.IsLeaf)
                    {
                        // Output current byte
                        output[decompressedBytesCount++] = ((HuffmanLeafNode)node).Symbol;
                        if (decompressedBytesCount == length)
                            break;

                        node = tree.Root;
                    }
                }
            }

            return (output, length, isLast);
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                isDisposed = true;
                if (gzipStream != null)
                    gzipStream.Dispose();
                if (inputStream != null)
                    inputStream.Dispose();
                if (outputStream != null)
                    outputStream.Dispose();
                GC.SuppressFinalize(this);
            }
        }
    }
}
