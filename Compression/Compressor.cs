﻿using Compression.Huffman;
using Compression.Utilities;
using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static Compression.Utilities.ByteSpanExtensions;

namespace Compression
{
    public class Compressor : IDisposable
    {
        public readonly byte[] Header = Encoding.UTF8.GetBytes("{huf;v1}");
        private const int defaultBufferSize = 1024 * 1024 + 512;
        private const int inputBufferSize = defaultBufferSize;
        private const int outputBufferSize = defaultBufferSize;
        private const int diskFetchSize = 1024 * 1024;
        private readonly Stream inputStream;
        private readonly Stream outputStream;
        private readonly GZipStream gzipStream;
        private readonly ArrayPool<byte> arrayPool;
        private bool isDisposed;

        public Compressor(string input, string output)
        {
            this.inputStream = File.Open(input, FileMode.Open, FileAccess.Read);
            this.outputStream = File.Create(output, defaultBufferSize);
            this.gzipStream = new(outputStream, CompressionLevel.NoCompression);
            this.arrayPool = ArrayPool<byte>.Shared;
        }

        public Compressor(Stream input, Stream output)
        {
            this.inputStream = input;
            this.outputStream = output;
            this.gzipStream = new(outputStream, CompressionLevel.NoCompression);
            this.arrayPool = ArrayPool<byte>.Shared;
        }

        public async Task Compress()
        {
            var threadsCount = Environment.ProcessorCount;
            var freeBufferIndices = new ConcurrentQueue<int>(Enumerable.Range(0, threadsCount * 2));
            var compressionQueue = new BlockingCollection<(byte[] Data, int Start, int Length, bool IsLast, int BufferIndex, int BlockIndex)>();
            var outputQueue = new BlockingCollection<(byte[] Data, int Start, int Length, int BufferIndex, int BlockIndex)>();
            var inputBuffer = arrayPool.Rent(defaultBufferSize * threadsCount * 2);
            var outputBuffer = arrayPool.Rent(defaultBufferSize * threadsCount * 2);

            // First part of the pipeline
            // Purpose: read individual blocks from file and place them into compressionQueue
            var readerTask = Task.Run(async () =>
            {
                var index = 0;
                var read = 0;
                do
                {
                    // Obtain free buffer index
                    if (!freeBufferIndices.TryDequeue(out var bufferIndex))
                        continue;
                    // Fetch next chunk
                    var blockIndex = index++;
                    var startIndex = bufferIndex * defaultBufferSize;
                    read = await inputStream.ReadAsync(inputBuffer.AsMemory(startIndex, diskFetchSize));
                    compressionQueue.Add((inputBuffer, startIndex, read, read < diskFetchSize, bufferIndex, blockIndex));

                } while (read == diskFetchSize);

                // Finish producer queue
                compressionQueue.CompleteAdding();

            });

            // Second part of the pipeline
            // Purpose: encode individual blocks and place compressed data into outputQueue 
            var compressionTask = Task.Run(() =>
            {
                var options = new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount };
                var partitioner = Partitioner.Create(compressionQueue.GetConsumingEnumerable(), EnumerablePartitionerOptions.NoBuffering);
                Parallel.ForEach(partitioner, options, arg =>
                {
                    var (data, start, length, isLast, bufferIndex, blockIndex) = arg;

                    // Process next chunk
                    var huffmanTree = BuildHuffmanTree(new ReadOnlySpan<byte>(data, start, length));
                    var compressed = CompressBlock(new Span<byte>(outputBuffer, start, diskFetchSize), new ReadOnlySpan<byte>(data, start, length), isLast, huffmanTree);
                    outputQueue.Add((outputBuffer, start, compressed.Length, bufferIndex, blockIndex));
                });

                // Finish producer queue
                outputQueue.CompleteAdding();
            });

            // Third part of the pipeline
            // Purpose: write individual compressed blocks into file
            var writeTask = Task.Run(async () =>
            {
                var lastWrittenBlockId = -1;
                // Synchronize blocks using a priority queue
                var priorityQueue = PriorityQueue<FileBlock>.CreateMinimum();

                // Write file header
                await gzipStream.WriteAsync(Header);

                foreach (var (data, start, length, bufferIndex, blockIndex) in outputQueue.GetConsumingEnumerable())
                {
                    // Store the block in the priority queue
                    priorityQueue.Enqueue(new(blockIndex, data, start, length, bufferIndex));

                    FileBlock? nextBlock = null;
                    do
                    {
                        nextBlock = null;
                        // Check if the next block is ready for writing
                        if (priorityQueue.Count > 0 && priorityQueue.Peek().Index == lastWrittenBlockId + 1)
                            nextBlock = priorityQueue.Dequeue();

                        if (nextBlock.HasValue)
                        {
                            // Write block header
                            var block = nextBlock!.Value;
                            var blockLength = block.Length;
                            var size1 = (byte)(blockLength & 0x0000_00FF);
                            var size2 = (byte)((blockLength & 0x0000_FF00) >> 8);
                            var size3 = (byte)((blockLength & 0x00FF_0000) >> 16);
                            var size4 = (byte)((blockLength & 0xFF00_0000) >> 24);
                            await gzipStream.WriteAsync(new[] { size1, size2, size3, size4 });

                            // Write data
                            await gzipStream.WriteAsync(block.Data.AsMemory(block.Start, block.Length));
                            arrayPool.Return(block.Data);
                            lastWrittenBlockId = block.Index;

                            // Zero buffer
                            Array.Clear(data, block.Start, block.Length);
                            // Return buffer
                            freeBufferIndices.Enqueue(block.BufferIndex);
                        }
                    } while (nextBlock.HasValue);
                }
            });

            // Wait for all tasks to finish and perform clean-up
            await Task.WhenAll(readerTask, compressionTask, writeTask);
            arrayPool.Return(inputBuffer);
            arrayPool.Return(outputBuffer);
        }

        internal static Span<byte> CompressBlock(Span<byte> output, ReadOnlySpan<byte> input, bool isLast, HuffmanTree tree)
        {
            // Get output buffer
            var spanIndex = 0;
            var bitIndex = 0;

            // Encode block header (data size)
            var blockLength = input.Length;
            MemoryMarshal.Write(output, ref blockLength);
            spanIndex += sizeof(uint);
            // Write isLastFlag
            output.WriteBit((isLast) ? BitValue.Low : BitValue.High, ref spanIndex, ref bitIndex);

            // Encode huffman tree
            WriteTree(output, ref spanIndex, ref bitIndex, tree);

            // Encode text block
            for (var index = 0; index < input.Length; index++)
            {
                var currentSymbol = input[index];
                var (encoding, length) = tree.GetTranslation(currentSymbol);
                
                // Write whole bytes
                while (length > 8)
                {
                    var currentByte = ((byte)encoding);
                    output.WriteByte(currentByte, ref spanIndex, ref bitIndex);
                    encoding >>= 8;
                    length -= 8;
                }

                // Write the rest
                if (length > 0)
                {
                    var currentByte = (byte)(encoding);
                    output.WriteBits(currentByte, length, ref spanIndex, ref bitIndex);
                }
            }

            return output[..(spanIndex + 1)];
        }

        internal static void WriteTree(Span<byte> output, ref int spanIndex, ref int bitIndex, HuffmanTree tree)
            => WriteNodeRecursively(output, ref spanIndex, ref bitIndex, tree.Root);

        private static void WriteNodeRecursively(Span<byte> output, ref int spanIndex, ref int bitIndex, IHuffmanNode node)
        {
            if (node is HuffmanInternalNode internalNode)
            {
                // Write 0 for internal nodes
                output.WriteBit(BitValue.Low, ref spanIndex, ref bitIndex);
                // Traverse children
                WriteNodeRecursively(output, ref spanIndex, ref bitIndex, internalNode.LeftChild);
                WriteNodeRecursively(output, ref spanIndex, ref bitIndex, internalNode.RightChild);
            }
            else
            {
                // Write 1 for leaf nodes and follow with byte value
                var leafNode = node as HuffmanLeafNode;
                output.WriteBit(BitValue.High, ref spanIndex, ref bitIndex);
                output.WriteByte(leafNode!.Symbol, ref spanIndex, ref bitIndex);
            }
        }

        private static HuffmanTree BuildHuffmanTree(ReadOnlySpan<byte> input)
        {
            Span<int> counters = stackalloc int[256];

            // Calculate occurrences for individual symbols
            for (var index = 0; index < input.Length; index++)
            {
                var currentSymbol = input[index];
                counters[currentSymbol]++;
            }

            // Build Huffman tree
            return new HuffmanTreeBuilder()
                .AddSymbolsInfo(counters)
                .Build();
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                isDisposed = true;
                if (inputStream != null)
                    inputStream.Dispose();
                if (gzipStream != null)
                    gzipStream.Dispose();
                if (outputStream != null)
                    outputStream.Dispose();
                GC.SuppressFinalize(this);
            }
        }
    }
}
